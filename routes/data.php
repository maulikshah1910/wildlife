<?php
Route::any('aliquet', 'DataTableController@aliquetData');
Route::any('animal-class', 'DataTableController@animalClassData');
Route::any('cell-line', 'DataTableController@cellLineData');
Route::any('cell-type', 'DataTableController@cellTypeData');
Route::any('common-name', 'DataTableController@commonNameData');
Route::any('note', 'DataTableController@noteData');
Route::any('species', 'DataTableController@speciesData');
Route::any('subgroup', 'DataTableController@subGroupData');
Route::any('tissue-type', 'DataTableController@tissueTypeData');
Route::any('enquiry', 'DataTableController@enquiryData');

Route::any('publication', 'DataTableController@publicationData');