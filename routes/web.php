<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return redirect('dashboard');
//});

Route::group(['namespace'=>'Site'], function(){
    Route::get('/', 'WebsiteController@getIndex')->name('front_page');
    Route::get('about', 'WebsiteController@getAbout')->name('about');
    Route::get('contact', 'WebsiteController@getContact')->name('contact');
    Route::get('search', 'WebsiteController@getSearch')->name('search');

    Route::post('search/filter', 'WebsiteController@postSearchFilter')->name('search-filter');

    Route::get('login', function(){
        return redirect('dashboard');
    });
    Route::get('register', function(){
        return redirect('dashboard');
    });
});

Route::get('app-logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth'], 'namespace' => 'Admin'], function (){
    Route::resource('dashboard', 'DashboardController');
    Route::resource('animal-class', 'AnimalClassController');
    Route::resource('subgroup', 'SubGroupController');
    Route::resource('species', 'SpeciesController');
    Route::resource('common-name', 'CommonNameController');
    Route::resource('tissue-type', 'TissueTypeController');
    Route::resource('cell-type', 'CellTypeController');
    Route::resource('notes', 'NoteController');
    Route::resource('aliquot', 'AliquetController');
    Route::resource('cell-line', 'CellLineController');
    Route::resource('publication', 'PublicationController');

    Route::get('profile', 'UserController@getProfile')->name('profile');
    Route::post('change-password', 'UserController@postChangePass')->name('change-password');
    Route::get('logout', 'DashboardController@logout');

    Route::get('enquiry', 'EnquiryController@index');
});