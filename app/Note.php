<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = [ 'title', 'link' ];
    protected  $hidden = [ 'created_at', 'updated_at', 'deleted_at' ];
}
