<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CellType extends Model
{
    protected $fillable = [ 'type' ];
    protected  $hidden = [ 'created_at', 'updated_at', 'deleted_at' ];
}
