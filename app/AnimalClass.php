<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnimalClass extends Model
{
    protected $fillable = [ 'class_name' ];
    protected  $hidden = [ 'created_at', 'updated_at', 'deleted_at' ];
}
