<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ContactEnquiry extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'email', 'message'];
    protected $hidden = [ 'deleted_at' ];
}
