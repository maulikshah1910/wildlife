<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aliquet extends Model
{
    protected $fillable = ['passenger_number', 'cryopreservation_date', 'status'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
