<?php

namespace App\Http\Controllers;

use App\Aliquet;
use App\AnimalClass;
use App\CellLine;
use App\CellType;
use App\CommonName;
use App\ContactEnquiry;
use App\Note;
use App\Publication;
use App\Species;
use App\SubGroup;
use App\TissueType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class DataTableController extends Controller
{
    /*
     * Get data for all available aliquets for datatable
     */
    public function aliquetData(Request $request){
        $table = DataTables::of(Aliquet::all());
        $table->addColumn('cellLineCount', function($aliquet){
            $id = $aliquet->id;
            $count = CellLine::where('aliquate_id', $id)->count();
            return $count;
        })->addColumn('action', function($aliquet){
            $id = $aliquet->id;
            $action = '<a href="'.route('aliquot.edit', $id).'" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>';
            return $action;
        })->editColumn('status', function($aliquet){
            $status = $aliquet->status == 1 ? '<label class="label label-success">In Stock</label>' : '<label class="label label-danger">Out of Stock</label>' ;
            return $status;
        });
        $table->rawColumns(['status', 'action']);
        return $table->make(true);
    }
    public function animalClassData(Request $request){
        $table = DataTables::of(AnimalClass::all());
        $table->addColumn('cellLineCount', function($animalClass){
            $id = $animalClass->id;
            $count = CellLine::where('animal_class_id', $id)->count();
            return $count;
        })->addColumn('action', function($animalClass){
            $id = $animalClass->id;
            $action = '<a href="'.route('animal-class.edit', $id).'" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>';
            return $action;
        });
        $table->rawColumns(['action']);
        return $table->make(true);
    }
    public function cellLineData(Request $request){
        $query = CellLine::allData();

        $table = DataTables::of($query);

        $table->addColumn('action', function($cellLine){
            $id = $cellLine->id;
            $action = '';

            if ( isset($cellLine->photo)
                || isset($cellLine->cell_culture_source)
                || isset($cellLine->culture_instructions)
                || isset($cellLine->dna_sequence)
                || isset($cellLine->karyotype)
                || isset($cellLine->mycoplasma_contamination)
                || isset($cellLine->transcriptomics)){

                $uploadDirURL = asset('/uploads/');

                $action .= '<a href="#" class="btn btn-xs btn-default " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="filesMenu-'.$cellLine->id.'"><i class="fa fa-eye"></i></a> ';
                $action .= '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">';
                if ( isset($cellLine->photo) ) {
                    $action .= '<a class="dropdown-item" target="_blank" href="' . asset('uploads/photos/'.$cellLine->photo) . '">View Photo</a>';
                }
                if ( isset($cellLine->cell_culture_source) ) {
                    $action .= '<a class="dropdown-item" target="_blank" href="' . asset('uploads/culture-sources/'.$cellLine->cell_culture_source) . '">View Cell Culture Source</a>';
                }
                if ( isset($cellLine->culture_instructions) ) {
                    $action .= '<a class="dropdown-item" target="_blank" href="' . asset('uploads/culture-instructions/'.$cellLine->culture_instructions) . '">View Culture Instructions</a>';
                }
                if ( isset($cellLine->dna_sequence) ) {
                    $action .= '<a class="dropdown-item" target="_blank" href="' . asset('uploads/dna-sequences/'.$cellLine->dna_sequence) . '">View DNA Sequence</a>';
                }
                if ( isset($cellLine->karyotype) ) {
                    $action .= '<a class="dropdown-item" target="_blank" href="' . asset('uploads/karyotypes/'.$cellLine->karyotype) . '">View Karyotype</a>';
                }
                if ( isset($cellLine->mycoplasma_contamination) ) {
                    $action .= '<a class="dropdown-item" target="_blank" href="' . asset('uploads/mycoplasma-contaminations/'.$cellLine->mycoplasma_contamination) . '">View Mycoplasma Contamination</a>';
                }
                if ( isset($cellLine->transcriptomics) ) {
                    $action .= '<a class="dropdown-item" target="_blank" href="' . asset('uploads/transcriptomics/'.$cellLine->transcriptomics) . '">View Transcriptomics</a>';
                }

                $action .= '</div>';
            }

            $publications = Publication::where('cell_line_id', $id)->get();
            if (count($publications) > 0){
                $action .= '<a href="#" class="btn btn-xs btn-default " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="publicationMenu-'.$cellLine->id.'"><i class="fa fa-link"></i></a> ';
                $action .= '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">';
                foreach ($publications as $p){
                    $action .= '<a class="dropdown-item" target="_blank" href="' . $p->link . '">' . $p->title . '</a>';
                }
                $action .= '</div>';
            }

            $action .= '<a class="btn btn-xs btn-primary" href="'.route('cell-line.edit', $cellLine->id).'"><i class="fa fa-pencil"></i></a> ';
            $action .= '<a class="btn btn-xs btn-warning btn-delete-cell-line" href="'.route('cell-line.destroy', $cellLine->id).'" data-id="'.$cellLine->id.'"><i class="fa fa-remove"></i></a>';

            return $action;
        });

        $table->rawColumns(['action']);

        return $table->make(true);
    }
    public function cellTypeData(Request $request){
        $table = DataTables::of(CellType::all());
        $table->addColumn('cellLineCount', function($cellType){
            $id = $cellType->id;
            $count = CellLine::where('cell_type_id', $id)->count();
            return $count;
        })->addColumn('action', function($cellType){
            $id = $cellType->id;
            $action = '<a href="'.route('cell-type.edit', $id).'" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>';
            return $action;
        });
        $table->rawColumns(['action']);
        return $table->make(true);
    }
    public function commonNameData(Request $request){
        $table = DataTables::of(CommonName::all());
        $table->addColumn('cellLineCount', function($commonName){
            $id = $commonName->id;
            $count = CellLine::where('common_name_id', $id)->count();
            return $count;
        })->addColumn('action', function($commonName){
            $id = $commonName->id;
            $action = '<a href="'.route('common-name.edit', $id).'" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>';
            return $action;
        });
        $table->rawColumns(['action']);
        return $table->make(true);
    }
    public function noteData(Request $request){
        $table = DataTables::of(Note::all());
        $table->addColumn('cellLineCount', function($note){
            $id = $note->id;
            $count = CellLine::where('note_id', $id)->count();
            return $count;
        })->addColumn('action', function($note){
            $id = $note->id;
            $action = '<a href="'.route('notes.edit', $id).'" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>';
            return $action;
        });
        $table->rawColumns(['action']);
        return $table->make(true);
    }
    public function speciesData(Request $request){
        $table = DataTables::of(Species::all());
        $table->addColumn('cellLineCount', function($species){
            $id = $species->id;
            $count = CellLine::where('species_id', $id)->count();
            return $count;
        })->addColumn('action', function($species){
            $id = $species->id;
            $action = '<a href="'.route('species.edit', $id).'" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>';
            return $action;
        });
        $table->rawColumns(['action']);
        return $table->make(true);
    }
    public function subGroupData(Request $request){
        $table = DataTables::of(SubGroup::all());
        $table->addColumn('cellLineCount', function($subgroup){
            $id = $subgroup->id;
            $count = CellLine::where('subgroup_id', $id)->count();
            return $count;
        })->addColumn('action', function($subgroup){
            $id = $subgroup->id;
            $action = '<a href="'.route('subgroup.edit', $id).'" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>';
            return $action;
        });
        $table->rawColumns(['action']);
        return $table->make(true);
    }
    public function tissueTypeData(Request $request){
        $table = DataTables::of(TissueType::all());
        $table->addColumn('cellLineCount', function($tissue){
            $id = $tissue->id;
            $count = CellLine::where('tissue_type_id', $id)->count();
            return $count;
        })->addColumn('action', function($tissue){
            $id = $tissue->id;
            $action = '<a href="'.route('tissue-type.edit', $id).'" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>';
            return $action;
        });
        $table->rawColumns(['action']);
        return $table->make(true);
    }
    public function enquiryData(Request $request){
        $table = DataTables::of(ContactEnquiry::all());

        $table->editColumn('created_at', function($enquiry){
            return Carbon::parse($enquiry->created_at)->format('d-M-Y h:i A');
        });

        return $table->make(true);
    }

    public function publicationData(Request $request){
        $table = DataTables::of(Publication::allPublications());

        $table->addColumn('action', function($publication){
            $id = $publication->id;
            $action = '';
            $action .= '<a href="'.route('publication.edit', $id).'" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>';
            return $action;
        });

        $table->rawColumns(['action']);
        return $table->make(true);
    }
}
