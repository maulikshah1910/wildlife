<?php

namespace App\Http\Controllers\Admin;

use App\Aliquet;
use App\AnimalClass;
use App\CellLine;
use App\CellType;
use App\CommonName;
use App\Note;
use App\Species;
use App\SubGroup;
use App\TissueType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get count for all items
        $animalClassCount = AnimalClass::all()->count();
        $subGroupCount = SubGroup::all()->count();
        $speciesCount = Species::all()->count();
        $commonNameCount = CommonName::all()->count();

        $tissueTypeCount = TissueType::all()->count();
        $cellTypeCount = CellType::all()->count();
        $notesCount = Note::all()->count();
        $aliquetCount = Aliquet::all()->count();

        $cellLineCount = CellLine::all()->count();

        return view('admin.dashboard', compact(
            'animalClassCount',
            'subGroupCount',
            'speciesCount',
            'commonNameCount',

            'tissueTypeCount',
            'cellTypeCount',
            'notesCount',
            'aliquetCount',

            'cellLineCount'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function logout(){
        Auth::logout();
        return redirect('/login');
    }
}
