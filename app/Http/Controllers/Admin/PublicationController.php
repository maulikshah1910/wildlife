<?php

namespace App\Http\Controllers\Admin;

use App\CellLine;
use App\Publication;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PublicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.publication.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cellLines = CellLine::orderBy('cell_line_label', 'ASC')->get();

        return view('admin.publication.form', compact('cellLines'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $publication = new Publication();
        $publication->title = $request->get('title');
        $publication->link = $request->get('link');
        $publication->cell_line_id = $request->get('cell_line');

        $publication->save();
        $request->session()->put('publication_create', true);
        return redirect(route('publication.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect(route('publication.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $publication = Publication::where('id', $id)->first();
        if (!isset($publication)){
            return redirect(route('publication.index'));
        }
        $cellLines = CellLine::orderBy('cell_line_label', 'ASC')->get();

        return view('admin.publication.form', compact('cellLines', 'publication'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $publication = Publication::where('id', $id)->first();
        if (!isset($publication)){
            return redirect(route('publication.index'));
        }
        $publication->title = $request->get('title');
        $publication->link = $request->get('link');
        $publication->cell_line_id = $request->get('cell_line');

        $publication->save();
        $request->session()->put('publication_update', true);
        return redirect(route('publication.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
