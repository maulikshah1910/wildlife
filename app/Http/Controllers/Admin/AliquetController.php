<?php

namespace App\Http\Controllers\Admin;

use App\Aliquet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AliquetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.aliquet.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.aliquet.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $aliquet = new Aliquet();
        $aliquet->passenger_number = $request->get('passenger_number');
        $aliquet->cryopreservation_date = $request->get('cryopreservation_date', Carbon::now()->format('Y-m-d'));
        $aliquet->status = $request->get('status', 1);
        $aliquet->save();

        $request->session()->put('aliquet_create', true);
        return redirect(route('aliquet.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return redirect(route('aliquet.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $aliquet = Aliquet::where('id', $id)->first();
        if ($aliquet == null){
            return redirect(route('aliquet.index'));
        }

        return view('admin.aliquet.form', compact('aliquet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $aliquet = Aliquet::where('id', $id)->first();
        if ($aliquet == null){
            return redirect(route('aliquet.index'));
        }

        $aliquet->passenger_number = $request->get('passenger_number');
        $aliquet->cryopreservation_date = $request->get('cryopreservation_date', Carbon::now()->format('Y-m-d'));
        $aliquet->status = $request->get('status', 1);
        $aliquet->save();

        $request->session()->put('aliquet_update', true);
        return redirect(route('aliquet.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
