<?php

namespace App\Http\Controllers\Admin;

use App\TissueType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TissueTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.tissue-type.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.tissue-type.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $type = $request->get('type');
        $tissueType = new TissueType();

        $tissueType->type = $type;
        $tissueType->save();

        $request->session()->put('tissuetype_create', true);
        return redirect(route('tissue-type.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return redirect(route('tissue-type.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tissueType = TissueType::where('id', $id)->first();
        if ($tissueType == null){
            return redirect(route('tissue-type.index'));
        }

        return view('admin.tissue-type.form', compact(
            'tissueType'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $tissueType = TissueType::where('id', $id)->first();
        if ($tissueType == null){
            return redirect(route('tissue-type.index'));
        }

        $tissueType->type = $request->get('type');
        $tissueType->save();

        $request->session()->put('tissuetype_update', true);
        return redirect(route('tissue-type.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
