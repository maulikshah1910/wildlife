<?php

namespace App\Http\Controllers\Admin;

use App\CommonName;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommonNameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.common-name.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.common-name.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $name = $request->get('name');
        $commonName = new CommonName();

        $commonName->name = $name;
        $commonName->save();

        $request->session()->put('commonname_create', true);
        return redirect(route('common-name.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return redirect(route('common-name.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $commonName = CommonName::where('id', $id)->first();
        if ($commonName == null){
            return redirect(route('common-name.index'));
        }

        return view('admin.common-name.form', compact(
            'commonName'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $commonName = CommonName::where('id', $id)->first();
        if ($commonName == null){
            return redirect(route('common-name.index'));
        }

        $commonName->name = $request->get('name');
        $commonName->save();

        $request->session()->put('commonname_update', true);
        return redirect(route('common-name.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
