<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    function getProfile(Request $request){
        return view('admin.profile.index');
    }

    function postChangePass(Request $request){
        $oldPass = $request->get('oldpass');
        $newPass = $request->get('newpass');
        $confPass = $request->get('confpass');

        $userID = Auth::user()->id;

        $user = User::where('id', $userID)->first();
        if (!isset($user)){
            return redirect('logout');
        }

        if (!Hash::check($oldPass, $user->password)){
            $request->session()->put('invalid_oldpass', true);
            return redirect(route('profile'));
        }

        if ($newPass != $confPass){
            $request->session()->put('password_mismatch', true);
            return redirect(route('profile'));
        }

        $user->password = bcrypt($newPass);
        $request->session()->put('password_update', true);
        return redirect(route('profile'));
    }
}
