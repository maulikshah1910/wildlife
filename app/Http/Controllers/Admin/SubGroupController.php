<?php

namespace App\Http\Controllers\Admin;

use App\SubGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.subgroup.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.subgroup.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $name = $request->get('name');
        $subgroup = new SubGroup();

        $subgroup->name = $name;
        $subgroup->save();

        $request->session()->put('subgroup_create', true);
        return redirect(route('subgroup.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return redirect(route('subgroup.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $subgroup = SubGroup::where('id', $id)->first();

        if ($subgroup == null){
            return redirect(route('subgroup.index'));
        }

        return view('admin.subgroup.form', compact(
            'subgroup'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $subgroup = SubGroup::where('id', $id)->first();

        if ($subgroup == null){
            return redirect(route('subgroup.index'));
        }

        $subgroup->name = $request->get('name');
        $subgroup->save();

        $request->session()->put('subgroup_update', true);
        return redirect(route('subgroup.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
