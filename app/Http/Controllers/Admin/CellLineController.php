<?php

namespace App\Http\Controllers\Admin;

use App\Aliquet;
use App\AnimalClass;
use App\CellLine;
use App\CellType;
use App\CommonName;
use App\Note;
use App\Species;
use App\SubGroup;
use App\TissueType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CellLineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.cell-line.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $animalClass = AnimalClass::orderBy('class_name', 'ASC')->get();
        $subgroup = SubGroup::orderBy('name', 'ASC')->get();
        $species = Species::orderBy('name', 'ASC')->get();
        $commonName = CommonName::orderBy('name', 'ASC')->get();
        $tissueType = TissueType::orderBy('type', 'ASC')->get();
        $cellType = CellType::orderBy('type', 'ASC')->get();
        $note = Note::orderBy('title', 'ASC')->get();
        $aliquet = Aliquet::orderBy('passenger_number', 'ASC')->get();

        $error = Session::has('error') ? Session::get('error') : null;
        if (Session::has('error')){
            Session::forget('error');
        }

        return view('admin.cell-line.form', compact(
            'animalClass',
            'subgroup',
            'species',
            'commonName',
            'tissueType',
            'cellType',
            'note',
            'aliquet',
            'error'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $cellLine = new CellLine();

            $animalClass = $request->get('animal_class');
            $cellLine->animal_class_id = $animalClass;

            $subGroup = $request->get('subgroup');
            $cellLine->subgroup_id = $subGroup;

            $species = $request->get('species');
            $cellLine->species_id = $species;

            $commonName = $request->get('common_name');
            $cellLine->common_name_id = $commonName;

            $cellType = $request->get('cell_type');
            $cellLine->cell_type_id = $cellType;

            $tissueType = $request->get('tissue_type');
            $cellLine->tissue_type_id = $tissueType;

            $note = $request->get('note');
            $cellLine->note_id = $note;

            $aliquet = $request->get('aliquet');
            $cellLine->aliquate_id = $aliquet;

            $label = $request->get('cell_line_label');
            $cellLine->cell_line_label = $label;

            $uploadDir = public_path() . '/uploads';
            $fileName = time();
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir);
            }
            if ($request->hasFile('photo')) {
                $fileDir = $uploadDir . '/photos';
                $file = $fileName . "." . $request->file('photo')->extension();
                $f = $request->file('photo');
                $f->move($fileDir, $file);

                $cellLine->photo = $file;
            }
            if ($request->hasFile('culture_source')) {
                $fileDir = $uploadDir . '/culture-sources';
                $file = $fileName . "." . $request->file('culture_source')->extension();
                $f = $request->file('culture_source');
                $f->move($fileDir, $file);

                $cellLine->cell_culture_source = $file;
            }
            if ($request->hasFile('culture_instructions')) {
                $fileDir = $uploadDir . '/culture-instructions';
                $file = $fileName . "." . $request->file('culture_instructions')->extension();
                $f = $request->file('culture_instructions');
                $f->move($fileDir, $file);

                $cellLine->culture_instructions = $file;
            }
            if ($request->hasFile('dna_seq')) {
                $fileDir = $uploadDir . '/dna-sequences';
                $file = $fileName . "." . $request->file('dna_seq')->extension();
                $f = $request->file('dna_seq');
                $f->move($fileDir, $file);

                $cellLine->dna_sequence = $file;
            }
            if ($request->hasFile('karyotype')) {
                $fileDir = $uploadDir . '/karyotypes';
                $file = $fileName . "." . $request->file('karyotype')->extension();
                $f = $request->file('karyotype');
                $f->move($fileDir, $file);

                $cellLine->karyotype = $file;
            }
            if ($request->hasFile('mycoplasma_contamination')) {
                $fileDir = $uploadDir . '/mycoplasma-contaminations';
                $file = $fileName . "." . $request->file('mycoplasma_contamination')->extension();
                $f = $request->file('mycoplasma_contamination');
                $f->move($fileDir, $file);

                $cellLine->mycoplasma_contamination = $file;
            }
            if ($request->hasFile('transcriptomics')) {
                $fileDir = $uploadDir . '/transcriptomics';
                $file = $fileName . "." . $request->file('transcriptomics')->extension();
                $f = $request->file('transcriptomics');
                $f->move($fileDir, $file);

                $cellLine->transcriptomics = $file;
            }

            $cellLine->save();

            $request->session()->put('cell_line_create', true);
            return redirect(route('cell-line.index'));
        } catch (\Exception $ex){
            $request->session()->put('error', $ex->getMessage());
            return redirect(route('cell-line.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return redirect(route('cell-line.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cellLine = CellLine::where('id', $id)->first();
        if (!isset($cellLine)){
            return redirect(route('cell-line.index'));
        }

        $animalClass = AnimalClass::orderBy('class_name', 'ASC')->get();
        $subgroup = SubGroup::orderBy('name', 'ASC')->get();
        $species = Species::orderBy('name', 'ASC')->get();
        $commonName = CommonName::orderBy('name', 'ASC')->get();
        $tissueType = TissueType::orderBy('type', 'ASC')->get();
        $cellType = CellType::orderBy('type', 'ASC')->get();
        $note = Note::orderBy('title', 'ASC')->get();
        $aliquet = Aliquet::orderBy('passenger_number', 'ASC')->get();

        $error = Session::has('error_update') ? Session::get('error_update') : null;
        if (Session::has('error_update')){
            Session::forget('error_update');
        }

        return view('admin.cell-line.form', compact(
            'cellLine',
            'animalClass',
            'subgroup',
            'species',
            'commonName',
            'tissueType',
            'cellType',
            'note',
            'aliquet',
            'error'
        ));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cellLine = CellLine::where('id', $id)->first();
        if (!isset($cellLine)){
            return redirect(route('cell-line.index'));
        }

        try{
            $animalClass = $request->get('animal_class');
            $cellLine->animal_class_id = $animalClass;

            $subGroup = $request->get('subgroup');
            $cellLine->subgroup_id = $subGroup;

            $species = $request->get('species');
            $cellLine->species_id = $species;

            $commonName = $request->get('common_name');
            $cellLine->common_name_id = $commonName;

            $cellType = $request->get('cell_type');
            $cellLine->cell_type_id = $cellType;

            $tissueType = $request->get('tissue_type');
            $cellLine->tissue_type_id = $tissueType;

            $note = $request->get('note');
            $cellLine->note_id = $note;

            $aliquet = $request->get('aliquet');
            $cellLine->aliquate_id = $aliquet;

            $label = $request->get('cell_line_label');
            $cellLine->cell_line_label = $label;

            $uploadDir = public_path() . '/uploads';
            $fileName = time();
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir);
            }
            if ($request->hasFile('photo')) {
                $fileDir = $uploadDir . '/photos';
                $file = $fileName . "." . $request->file('photo')->extension();
                $f = $request->file('photo');
                $f->move($fileDir, $file);

                $cellLine->photo = $file;
            }
            if ($request->hasFile('culture_source')) {
                $fileDir = $uploadDir . '/culture-sources';
                $file = $fileName . "." . $request->file('culture_source')->extension();
                $f = $request->file('culture_source');
                $f->move($fileDir, $file);

                $cellLine->cell_culture_source = $file;
            }
            if ($request->hasFile('culture_instructions')) {
                $fileDir = $uploadDir . '/culture-instructions';
                $file = $fileName . "." . $request->file('culture_instructions')->extension();
                $f = $request->file('culture_instructions');
                $f->move($fileDir, $file);

                $cellLine->culture_instructions = $file;
            }
            if ($request->hasFile('dna_seq')) {
                $fileDir = $uploadDir . '/dna-sequences';
                $file = $fileName . "." . $request->file('dna_seq')->extension();
                $f = $request->file('dna_seq');
                $f->move($fileDir, $file);

                $cellLine->dna_sequence = $file;
            }
            if ($request->hasFile('karyotype')) {
                $fileDir = $uploadDir . '/karyotypes';
                $file = $fileName . "." . $request->file('karyotype')->extension();
                $f = $request->file('karyotype');
                $f->move($fileDir, $file);

                $cellLine->karyotype = $file;
            }
            if ($request->hasFile('mycoplasma_contamination')) {
                $fileDir = $uploadDir . '/mycoplasma-contaminations';
                $file = $fileName . "." . $request->file('mycoplasma_contamination')->extension();
                $f = $request->file('mycoplasma_contamination');
                $f->move($fileDir, $file);

                $cellLine->mycoplasma_contamination = $file;
            }
            if ($request->hasFile('transcriptomics')) {
                $fileDir = $uploadDir . '/transcriptomics';
                $file = $fileName . "." . $request->file('transcriptomics')->extension();
                $f = $request->file('transcriptomics');
                $f->move($fileDir, $file);

                $cellLine->transcriptomics = $file;
            }

            $cellLine->save();
            $request->session()->put('cell_line_update', true);
            return redirect(route('cell-line.index'));
        } catch(\Exception $ex){
            $request->session()->put('error', $ex->getMessage());
            return redirect(route('cell-line.edit', $id));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $cellLine = CellLine::where('id', $id)->first();
            if (!isset($cellLine)) {
                return response()->json(['success' => false, 'message' => 'Cell Line not found'], 200);
            }
            $cellLine->delete();
            return response()->json(['success'=>true], 200);
        } catch (\Exception $ex){
            return response()->json(['success'=>false, 'nessage'=>$ex->getMessage()], 200);
        }
    }
}
