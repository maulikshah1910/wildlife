<?php

namespace App\Http\Controllers\Admin;

use App\CellType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CellTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.cell-type.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.cell-type.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $type = $request->get('type');
        $cellType = new CellType();

        $cellType->type = $type;
        $cellType->save();

        $request->session()->put('celltype_create', true);
        return redirect(route('cell-type.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return redirect(route('cell-type.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cellType = CellType::where('id', $id)->first();
        if ($cellType == null){
            return redirect(route('cell-type.index'));
        }

        return view('admin.cell-type.form', compact(
            'cellType'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $cellType = CellType::where('id', $id)->first();
        if ($cellType == null){
            return redirect(route('cell-type.index'));
        }

        $cellType->type = $request->get('type');
        $cellType->save();

        $request->session()->put('celltype_update', true);
        return redirect(route('cell-type.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
