<?php

namespace App\Http\Controllers\Admin;

use App\AnimalClass;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AnimalClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.animal-class.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.animal-class.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $className = $request->get('class_name');
        $animalClass = new AnimalClass();

        $animalClass->class_name = $className;
        $animalClass->save();

        $request->session()->put('animal_class_create', true);
        return redirect(route('animal-class.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return redirect(route('animal-class.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $animalClass = AnimalClass::where('id', $id)->first();
        if ($animalClass == null){
            return redirect(route('animal-class.index'));
        }

        return view('admin.animal-class.form', compact(
            'animalClass'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $animalClass = AnimalClass::where('id', $id)->first();
        if ($animalClass == null){
            return redirect(route('animal-class.index'));
        }

        $animalClass->class_name = $request->get('class_name');
        $animalClass->save();

        $request->session()->put('animalclass_update', true);
        return redirect(route('animal-class.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
