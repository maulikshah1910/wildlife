<?php

namespace App\Http\Controllers\Site;

use App\CellLine;
use App\ContactEnquiry;
use App\Publication;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class WebsiteController extends Controller
{
    //
    function getIndex(Request $request){
        return view('website.index');
    }
    function getAbout(Request $request){
        return view('website.about');
    }
    function getContact(Request $request){
        return view('website.contact');
    }
    function getSearch(Request $request){
        return view('website.search');
    }
    function postSearchFilter(Request $request){
        $search = $request->get('query');
        $query = CellLine::searchResults($search);

        $table = DataTables::of($query);

        $table->addColumn('actions', function($cellLine){
            $action = '';

            if ( isset($cellLine->photo)
                || isset($cellLine->cell_culture_source)
                || isset($cellLine->culture_instructions)
                || isset($cellLine->dna_sequence)
                || isset($cellLine->karyotype)
                || isset($cellLine->mycoplasma_contamination)
                || isset($cellLine->transcriptomics)){
                $action .= '<a href="#" class="btn btn-xs btn-default " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="filesMenu-'.$cellLine->id.'"><i class="fa fa-eye"></i></a> ';
                $action .= '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">';
                if ( isset($cellLine->photo) ) {
                    $action .= '<a class="dropdown-item" target="_blank" href="' . asset('uploads/photos/'.$cellLine->photo) . '">View Photo</a>';
                }
                if ( isset($cellLine->cell_culture_source) ) {
                    $action .= '<a class="dropdown-item" target="_blank" href="' . asset('uploads/culture-sources/'.$cellLine->cell_culture_source) . '">View Cell Culture Source</a>';
                }
                if ( isset($cellLine->culture_instructions) ) {
                    $action .= '<a class="dropdown-item" target="_blank" href="' . asset('uploads/culture-instructions/'.$cellLine->culture_instructions) . '">View Culture Instructions</a>';
                }
                if ( isset($cellLine->dna_sequence) ) {
                    $action .= '<a class="dropdown-item" target="_blank" href="' . asset('uploads/dna-sequences/'.$cellLine->dna_sequence) . '">View DNA Sequence</a>';
                }
                if ( isset($cellLine->karyotype) ) {
                    $action .= '<a class="dropdown-item" target="_blank" href="' . asset('uploads/karyotypes/'.$cellLine->karyotype) . '">View Karyotype</a>';
                }
                if ( isset($cellLine->mycoplasma_contamination) ) {
                    $action .= '<a class="dropdown-item" target="_blank" href="' . asset('uploads/mycoplasma-contaminations/'.$cellLine->mycoplasma_contamination) . '">View Mycoplasma Contamination</a>';
                }
                if ( isset($cellLine->transcriptomics) ) {
                    $action .= '<a class="dropdown-item" target="_blank" href="' . asset('uploads/transcriptomics/'.$cellLine->transcriptomics) . '">View Transcriptomics</a>';
                }

                $action .= '</div>';
            }

            return $action;
        })->addColumn('publications', function($cellLine){
            $id = $cellLine->id;
            $publications = Publication::where('cell_line_id', $id)->get();
            $result = '';
            foreach ($publications as $p){
                $result .= '<p><a href="'.$p->link.'" target="_blank">'.$p->title.'</a></p>';
            }
            return $result;
        });

        $table->rawColumns(['actions', 'publications']);

        return $table->make(true);
    }

    function postSubmitEnquiry(Request $request){
        try {
            $name = $request->get('name');
            $email = $request->get('email');
            $message = $request->get('message');

            $enq = new ContactEnquiry();
            $enq->name = $name;
            $enq->email = $email;
            $enq->message = $message;

            $enq->save();
            return response()->json([ 'success'=>true ]);
        } catch (\Exception $ex){
            return response()->json([ 'success'=>false, 'message'=>$ex->getMessage() ]);
        }

    }
}
