<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CellLine extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'animal_class_id', 'subgroup_id', 'species_id', 'common_name_id', 'tissue_type_id', 'cell_type_id', 'note_id',
        'aliquate_id', 'cell_line_label', 'photo',
        'cell_culture_source', 'culture_instructions', 'dna_sequence', 'karyotype', 'mycoplasma_contamination', 'transcriptomics'
    ];
    protected  $hidden = [ 'created_at', 'updated_at', 'deleted_at' ];

    protected function allData(){
        $query = CellLine::leftJoin('animal_classes as ac', 'ac.id', '=', 'cell_lines.animal_class_id')
            ->leftJoin('sub_groups as sg', 'sg.id', '=', 'cell_lines.subgroup_id')
            ->leftJoin('species as sp', 'sp.id', '=', 'cell_lines.species_id')
            ->leftJoin('common_names as cn', 'cn.id', '=', 'cell_lines.common_name_id')
            ->leftJoin('cell_types as ct', 'ct.id', '=', 'cell_lines.cell_type_id')
            ->leftJoin('tissue_types as tt', 'tt.id', '=', 'cell_lines.tissue_type_id')
            ->leftJoin('notes as n', 'n.id', '=', 'cell_lines.note_id')
            ->leftJoin('aliquets as a', 'a.id', '=', 'cell_lines.aliquate_id')
            ->select('cell_lines.*', 'ac.class_name as animal_class', 'sg.name as subgroup', 'sp.name as species',
                'cn.name as common_name', 'tt.type as tissue_type', 'ct.type as cell_type', 'n.title as note_title',
                'n.link as note_link', 'a.passenger_number as aliquate_passenger_number', 'a.cryopreservation_date as aliquate_cryopreservation_date');

        return $query;
    }
    protected function searchResults($search){
        $query = CellLine::leftJoin('animal_classes as ac', 'ac.id', '=', 'cell_lines.animal_class_id')
            ->leftJoin('sub_groups as sg', 'sg.id', '=', 'cell_lines.subgroup_id')
            ->leftJoin('species as sp', 'sp.id', '=', 'cell_lines.species_id')
            ->leftJoin('common_names as cn', 'cn.id', '=', 'cell_lines.common_name_id')
            ->leftJoin('cell_types as ct', 'ct.id', '=', 'cell_lines.cell_type_id')
            ->leftJoin('tissue_types as tt', 'tt.id', '=', 'cell_lines.tissue_type_id')
            ->where('cell_lines.cell_line_label', 'LIKE', '%'.$search.'%')
            ->orWhere('sg.name', 'LIKE', '%'.$search.'%')
            ->orWhere('sp.name', 'LIKE', '%'.$search.'%')
            ->orWhere('cn.name', 'LIKE', '%'.$search.'%')
            ->orWhere('ct.type', 'LIKE', '%'.$search.'%')
            ->orWhere('tt.type', 'LIKE', '%'.$search.'%')
            ->select('cell_lines.*', 'ac.class_name as animal_class', 'sg.name as subgroup', 'sp.name as species',
                'cn.name as common_name', 'tt.type as tissue_type', 'ct.type as cell_type' );

        return $query;
    }
}
