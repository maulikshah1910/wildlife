<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Publication extends Model
{
    use SoftDeletes;
    //
    protected $fillable = [ 'title', 'link', 'cell_line_id' ];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public static function allPublications(){
        return Publication::leftJoin('cell_lines as cl', 'cl.id', '=', 'publications.cell_line_id')
                    ->select('publications.*', 'cl.cell_line_label');
    }
}
