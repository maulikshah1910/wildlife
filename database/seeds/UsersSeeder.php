<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
//            [
//                'name' => 'System Admin',
//                'email' => 'maulik.shah1910@gmail.com',
//                'password' => bcrypt('maulik'),
//            ],
//            [
//                'name' => 'Nisarg Shah',
//                'email' => 'nisargbshah@gmail.com',
//                'password' => bcrypt('nisarg'),
//            ],
            [
                'name' => 'Admin',
                'email' => 'admin',
                'password' => bcrypt('admin'),
            ],
        ];
        //
        DB::table('users')->insert( $users );
    }
}
