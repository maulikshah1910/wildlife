<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCellLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cell_lines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('animal_class_id')->unsigned();
            $table->bigInteger('subgroup_id')->unsigned();
            $table->bigInteger('species_id')->unsigned();
            $table->bigInteger('common_name_id')->unsigned();
            $table->bigInteger('tissue_type_id')->unsigned();
            $table->bigInteger('cell_type_id')->unsigned();
            $table->string('cell_line_label', 10);
            $table->text('photo')->nullable();
            $table->text('cell_culture_source')->nullable()->comment('PDF File');
            $table->text('culture_instructions')->nullable()->comment('PDF File');
            $table->text('dna_sequence')->nullable()->comment('PDF File');
            $table->text('karyotype')->nullable()->comment('PDF File');
            $table->text('mycoplasma_contamination')->nullable()->comment('PDF File');
            $table->text('transcriptomics')->nullable()->comment('PDF File');
            $table->bigInteger('note_id')->unsigned();
            $table->bigInteger('aliquate_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('animal_class_id')->references('id')->on('animal_classes')->onDelete('cascade');
            $table->foreign('subgroup_id')->references('id')->on('sub_groups')->onDelete('cascade');
            $table->foreign('species_id')->references('id')->on('species')->onDelete('cascade');
            $table->foreign('common_name_id')->references('id')->on('common_names')->onDelete('cascade');
            $table->foreign('tissue_type_id')->references('id')->on('tissue_types')->onDelete('cascade');
            $table->foreign('cell_type_id')->references('id')->on('cell_types')->onDelete('cascade');

            $table->foreign('note_id')->references('id')->on('notes')->onDelete('cascade');
            $table->foreign('aliquate_id')->references('id')->on('aliquets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cell_lines');
    }
}
