<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAliquetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aliquets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('passenger_number');
            $table->timestamp('cryopreservation_date');
            $table->integer('status')->comment('1=>In Stock, 2=>Out of Stock');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aliquets');
    }
}
