<?php
return [
    "title" => "WildLife Admin Panel",
    "subtitle" => "Login to gain access",
    "fields" => [
        "user" => "Email",
        "pass" => "Password"
    ],
    "submit" => "Login"
];