@extends('layouts.website-template')

@section('page-title', 'Search')

@section('breadcrumbs')
    <p>home <i class="ti-angle-right"></i> Search</p>
    <h2>Search</h2>
@endsection

@section('page-styles')
    <link href="{{ asset('front/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('page-scripts')
    <script src="{{ asset('front/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('front/js/dataTables.bootstrap4.min.js') }}"></script>

    <script>
        $(document).on('submit', '#frmSearch', function(e){
            e.preventDefault();
            if ($('#reset-row').hasClass('d-none')){
                $('#reset-row').removeClass('d-none');
            }
            loadTable();
        });
        function loadTable(){
            $tbl = $('#tblSearch').dataTable({
                processing: true,
                serverSide: true,
                cache: false,
                searching: false,
                paging: false,
                destroy: true,
                iDisplayLength: 100,
                ajax: {
                    url: '{{ route('search-filter') }}',
                    type: "POST",
                    dataSrc: "data",
                    data: {
                        _token: '{{ csrf_token() }}',
                        query: $('#search-text').val()
                    }
                },
                columns: [
                    { data: "id", searchable: false, sortable: false, visible: false },
                    { data: "animal_class", name: 'ac.class_name' },
                    { data: "subgroup", name: 'sg.name' },
                    { data: "species", name: 'sp.name' },
                    { data: "common_name", name: 'cn.name' },
                    { data: "tissue_type", name: 'tt.type' },
                    { data: "cell_type", name: 'ct.type' },
                    { data: "cell_line_label" },
                    { data: "publications", searchable: false, sortable: false },
                    { data: "actions", sortable: false, searchable: false },
                ],
                preDrawCallback: function(){
                    $('#loader').removeClass('d-none');
                    $('#search-results').addClass('d-none');
                },
                drawCallback: function(){
                    $('#loader').addClass('d-none');
                    $('#search-results').removeClass('d-none');
                }
            });
        }

        $(document).on('click', '#btn-reset', function(e){
            $('#frmSearch').trigger('reset');
            $('#reset-row').addClass('d-none');
            loadTable();
        });
    </script>
@endsection

@section('page-content')
    <section class="about_part section_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="search-box">
                        <form action="#" type="post" class="form-horizontal" id="frmSearch">
                            <div class="row" id="search-row">
                                <div class="col-md-10">
                                    <input id="search-text" type="text" class="form-control" placeholder="Search text here..." />
                                </div>
                                <div class="col-md-2 text-right">
                                    <button type="submit" class="btn btn-primary"> Search <i class="fa fa-search"></i></button>
                                </div>
                            </div>
                            <div class="row mt-3 d-none" id="reset-row">
                                <div class="col-md-12 text-center">
                                    <button type="button" id="btn-reset" class="btn btn-danger">Reset <i class="fa fa-refresh"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row d-none" id="loader">
                <div class="col-md-12 text-center">
                    <i class="fa fa-5x fa-spin fa-spinner"></i>
                </div>
            </div>
            <div class="row d-none" id="search-results">
                <div class="col-md-12">
                    <div id="search-result">
                        <table class="table table-bordered table-hover table-striped" id="tblSearch">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Animal Class</th>
                                <th>SubGroup</th>
                                <th>Species Name</th>
                                <th>Common Name</th>
                                <th>Tissue Type</th>
                                <th>Cell Type</th>
                                <th>Cell Line Label</th>
                                <th>Associated Publications</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection