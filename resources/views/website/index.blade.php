@extends('layouts.website-template')

@section('page-title', 'Home')

@section('page-styles')
    <style>
        .banner_part {
            background-image: url('{{ asset('images/banner-bg.webp') }}');
            background-size: cover;
            background-attachment: fixed;
        }
        .about_part_img img { width: 100%; }
    </style>
@endsection

@section('page-scripts')
    <script>
        $(document).on('submit', '.contact_form', function(e){
            var allow = true;

            $('#alert').html();
            $('.contact_form').find('.form-group').removeClass('has-error');
            $('.contact_form').find('.form-group .error').remove();

            if ($('#name').val() == ""){
                allow = false;
                $('#name').parent().addClass('has-error');
                $('#name').parent().append('<label class="error">Please Enter Your Name</label>');
            }

            if ($('#email').val() == ""){
                allow = false;
                $('#email').parent().addClass('has-error');
                $('#email').parent().append('<label class="error">Please Enter Your Email</label>');
            } else {
                var email = $('#email').val();
                var mailFormat = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

                if (!mailFormat.test(email)){
                    allow = false;
                    $('#email').parent().addClass('has-error');
                    $('#email').parent().append('<label class="error">Please Enter Valid Email</label>');
                }
            }

            if ( $('#message').val() == "" ) {
                allow = false;
                $('#message').parent().addClass('has-error');
                $('#message').parent().append('<label class="error">Please Enter Message</label>');
            }

            e.preventDefault();
            if (allow){
                $.ajax({
                    type: "POST",
                    url: '{{ route('contact-enquiry') }}',
                    data: $('#contactForm').serialize(),
                    success: function(response){
                        console.log(response);
                        var alert = "";
                        if (response.success){
                            alert += '<div class="alert alert-success alert-dismissable">' +
                                        '<button class="close" data-dismiss="alert"><i class="ti-close"></i></button>' +
                                        '<strong>Success...!</strong><br />' +
                                        'Your enquiry is submitted successfully...' +
                                '   </div>';
                            $('#contactForm')[0].reset();
                        } else {
                            alert += '<div class="alert alert-warning alert-dismissable">' +
                                        '<button class="close" data-dismiss="alert"><i class="ti-close"></i></button>' +
                                        '<strong>Error...!</strong><br />' +
                                        response.message +
                                    '</div>';
                        }

                        $('#alert').html(alert);
                    }
                });
            }
        })
    </script>
@endsection

@section('page-content')
    <section class="about_part section_padding">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-6">
                    <div class="about_part_text">
                        <h5>About Us</h5>
                        <h2></h2>
                        <p>
                            The ‘ARI-TOX Marine Wildlife Cell Bank’ is Australia’s first marine wildlife cell culture bank - for
                            cetaceans (dolphins and whales), pinnipeds (seals), dugongs, reptiles (turtles and crocodiles) and
                            elasmobranchs (sharks and rays). These cell cultures will provide a platform for the international
                            scientific community to research these often threatened animals, thereby enhancing management
                            and conservation of Australian marine wildlife.
                        </p>
                        <p>
                            The ARI-TOX Marine Wildlife Cell Bank is housed at the Griffith University G51 Research
                            Laboratories. All cultures are stored between -140 and -180 °C in the vapour phase of liquid
                            nitrogen. The establishment and characterisation of the initial 25 cell cultures from sea turtles,
                            cetaceans and dugong was funded by the Sea World Research and Rescue Foundation Inc. and
                            Griffith University.
                        </p>
                        <p>
                            Upon request, cell cultures will be made available to researchers around the world. Simply search
                            the database, add the cultures you wish to request to your ‘cart’ and upload a proforma detailing
                            the objectives of your research and expected outcomes. Packaging and transport of the samples will
                            be arranged by the ARI-TOX Marine Wildlife Sample Bank staff, and all costs of transport will be at
                            the expense of the requester.
                        </p>
                        <p class="text-center">
                            <a class="btn btn-primary" href="{{ route('search') }}"><i class="fa fa-search"></i> Search</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="about_part_img">
                        <img src="{{ asset('images/turtle.jpg') }}" alt="" />
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="contact-section section_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="contact-title">Get in Touch</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="alert"></div>
                        </div>
                    </div>
                    <form class="form-horizontal form-contact contact_form" action="#" method="post" id="contactForm" novalidate="novalidate">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control "
                                        name="name"
                                        id="name"
                                        type="text" placeholder="Enter your name" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control "
                                           name="email"
                                           id="email"
                                           type="email" placeholder="Enter email address" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea class="form-control w-100 "
                                          name="message"
                                          id="message"
                                          cols="30"
                                          rows="9" placeholder="Enter Message"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12 ">
                                <button type="submit" class="button button-contactForm btn_1">Send Message</button>
                                <span class="ti-reload ml-3 d-none"></span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-4">
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-user"></i></span>
                        <div class="media-body">
                            <h3>Dr Jason van de Merwe.</h3>
                        </div>
                    </div>
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                        <div class="media-body">
                            <h3>+61 7 5552 8949</h3>
                        </div>
                    </div>
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-email"></i></span>
                        <div class="media-body">
                            <h3>j.vandemerwe@griffith.edu.au</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection