
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login | {{ config('app.name', 'Wild Life') }}</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet" />

    <link href="{{ asset('css/animate.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" />
</head>
<body class="text-white">
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h2>{{ trans('login.title') }}</h2>
            <h3>{{ trans('login.subtitle') }}</h3>
        </div>

        <form class="m-t" role="form" method="post" action="{{ route('login') }}">
            @csrf
            @if ($errors->has('email'))
                <div class="form-group">
                    <label class="label label-danger">{{$errors->first()}}</label>
                </div>
            @endif
            <div class="form-group">
                <input type="test" style="color: #000" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="{{ trans('login.fields.user') }}" />
            </div>
            <div class="form-group">
                <input id="password" style="color: #000" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="{{ trans('login.fields.pass') }}" />
            </div>
            <button type="submit" class="btn btn-primary">{{ trans('login.submit') }}</button>
        </form>
    </div>
</div>
</body>
<!-- Mainly scripts -->
<script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.js') }}"></script>

</html>
