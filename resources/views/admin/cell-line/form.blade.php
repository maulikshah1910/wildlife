@extends('layouts.admin-panel')

@section('title', 'Cell Lines')

@section('page_styles')
    <link rel="stylesheet" href="{{ asset('css/plugins/select2/select2.min.css') }}" />

    <style>
        #frm label.error {
            position: absolute;
            bottom: 0;
            top: auto;
            right: 2.5%;
            z-index: 999;
            font-weight: 600;
        }
    </style>
@endsection

@section('page_scripts')
    <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
    <script>
        $('#frm').find('select').select2();
        $('#frm').validate({
            rules: {
                animal_class:{
                    required: true,
                },
                subgroup: {
                    required: true
                },
                species: {
                    required: true
                },
                common_name:{
                    required: true
                },
                tissue_type: {
                    required: true,
                },
                cell_type: {
                    required: true
                },
                cell_line_label: {
                    required: true
                },
                note: {
                    required: true
                },
                aliquet: {
                    required: true
                },
                photo: {
                    extension: "jpg|jpeg|png",
                    filesize: 2
                },
                culture_source:{
                    extension: 'pdf',
                    filesize: 2
                },
                culture_instructions:{
                    extension: 'pdf',
                    filesize: 2
                },
                dna_seq:{
                    extension: 'pdf',
                    filesize: 2
                },
                karyotype:{
                    extension: 'pdf',
                    filesize: 2
                },
                mycoplasma_contamination:{
                    extension: 'pdf',
                    filesize: 2
                },
                transcriptomics:{
                    extension: 'pdf',
                    filesize: 2
                },
            }
        });
        $.validator.addMethod('filesize', function (value, element, param) {
            var fileBytes = parseInt(param) * 1024 * 1024;
            return this.optional(element) || (element.files[0].size <= fileBytes)
        }, 'File size must be less than {0} MB');

    </script>
@endsection

@section('page_header')
    <h1>Cell Lines</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('cell-line.index') }}">Cell Lines</a></li>
        <li class="breadcrumb-item active"><strong>{{ isset($cellLine) ? "Edit Cell Line" : "Create Cell Line" }}</strong></li>
    </ol>
@endsection

@section('page_content')
    @if (isset($error))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissable">
                    <strong>Error...</strong>
                    {{ $error }}
                    <button data-dismiss="alert" class="close">&times;</button>
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>{{ isset($cellLine) ? "Edit Cell Line" : "Create Cell Line" }}</h5>
                    <div class="ibox-tools">
                        <a href="{{ route('cell-line.index') }}">
                            <i class="fa fa-long-arrow-left"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" id="frm" action="{{ isset($cellLine) ? route('cell-line.update', $cellLine->id) : route('cell-line.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @if (isset($cellLine))
                            {{ method_field('PUT') }}
                            <input type="hidden" name="id" value="{{ $cellLine->id }}" />
                        @endif
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Animal Class: </label>
                            <div class="col-md-9">
                                <select name="animal_class" id="animal_class" class="form-control">
                                    <option value="">---</option>
                                    @foreach ($animalClass as $ac)
                                        <option @if (isset($cellLine) && $cellLine->animal_class_id == $ac->id) {{ 'selected' }} @endif value="{{ $ac->id }}">{{ $ac->class_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Sub Group: </label>
                            <div class="col-md-9">
                                <select name="subgroup" id="subgroup" class="form-control">
                                    <option value="">---</option>
                                    @foreach ($subgroup as $sg)
                                        <option @if(isset($cellLine) && $cellLine->subgroup_id == $sg->id) {{ 'selected' }} @endif value="{{ $sg->id }}">{{ $sg->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Species: </label>
                            <div class="col-md-9">
                                <select name="species" id="species" class="form-control">
                                    <option value="">---</option>
                                    @foreach ($species as $sp)
                                        <option @if(isset($cellLine) && $cellLine->species_id == $sp->id) {{ 'selected' }} @endif value="{{ $sp->id }}">{{ $sp->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Common Name: </label>
                            <div class="col-md-9">
                                <select name="common_name" id="common_name" class="form-control">
                                    <option value="">---</option>
                                    @foreach ($commonName as $cn)
                                        <option @if(isset($cellLine) && $cellLine->common_name_id == $cn->id) {{ 'selected' }} @endif value="{{ $cn->id }}">{{ $cn->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Tissue Type: </label>
                            <div class="col-md-9">
                                <select name="tissue_type" id="tissue_type" class="form-control">
                                    <option value="">---</option>
                                    @foreach ($tissueType as $t)
                                        <option @if(isset($cellLine) && $cellLine->tissue_type_id == $t->id) {{ 'selected' }} @endif value="{{ $t->id }}">{{ $t->type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Cell Type: </label>
                            <div class="col-md-9">
                                <select name="cell_type" id="cell_type" class="form-control">
                                    <option value="">---</option>
                                    @foreach ($cellType as $c)
                                        <option @if(isset($cellLine) && $cellLine->cell_type_id == $c->id) {{ 'selected' }} @endif value="{{ $c->id }}">{{ $c->type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Cell Line Label: </label>
                            <div class="col-md-9">
                                <input type="text" name="cell_line_label" id="cell_line_label" class="form-control" value="{{ isset($cellLine) ? $cellLine->cell_line_label : "" }}">
                            </div>
                        </div>

                        <div class="row form-group">
                            <label for="photo" class="col-md-3 col-form-label">Photo: <span class="text-warning">JPEG or PNG image upto 2 MB</span> </label>
                            <div class="col-md-9">
                                @if ( isset($cellLine->photo) )
                                    <div class="input-group">
                                        <input type="file" name="photo" id="photo" class="form-control">
                                        <div class="input-group-addon">
                                            <a target="_blank" href="{{ asset('uploads/photos/'.$cellLine->photo) }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <input type="file" name="photo" id="photo" class="form-control">
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Cell Culture Source: <span class="text-warning">PDF upto 2 MB</span> </label>
                            <div class="col-md-9">
                                @if (isset($cellLine->cell_culture_source))
                                    <div class="input-group">
                                        <input type="file" name="culture_source" id="culture_source" class="form-control">
                                        <div class="input-group-addon">
                                            <a href="{{ asset('uploads/culture-sources/'.$cellLine->cell_culture_source) }}" target="_blank">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <input type="file" name="culture_source" id="culture_source" class="form-control">
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Culture Instructions: <span class="text-warning">PDF upto 2 MB</span> </label>
                            <div class="col-md-9">
                                @if (isset($cellLine->culture_instructions))
                                    <div class="input-group">
                                        <input type="file" name="culture_instructions" id="culture_instructions" class="form-control">
                                        <div class="input-group-addon">
                                            <a target="_blank" href="{{ asset('uploads/culture-sources/'.$cellLine->culture_instructions) }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <input type="file" name="culture_instructions" id="culture_instructions" class="form-control">
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">DNA Sequence: <span class="text-warning">PDF upto 2 MB</span> </label>
                            <div class="col-md-9">
                                @if (isset($cellType->dna_sequence))
                                    <div class="input-group">
                                        <input type="file" name="dna_seq" id="dna_seq" class="form-control">
                                        <div class="input-group-addon">
                                            <a href="{{ asset('uploads/dna-sequences/'.$cellLine->dna_sequence) }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <input type="file" name="dna_seq" id="dna_seq" class="form-control">
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Karyotype: <span class="text-warning">PDF upto 2 MB</span> </label>
                            <div class="col-md-9">
                                @if(isset($cellLine->karyotype))
                                    <div class="input-group">
                                        <input type="file" name="karyotype" id="karyotype" class="form-control">
                                        <div class="input-group-addon">
                                            <a target="_blank" href="{{ asset('uploads/karyotypes/'.$cellLine->karyotype) }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <input type="file" name="karyotype" id="karyotype" class="form-control">
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Mycoplasma Contamination: <span class="text-warning">PDF upto 2 MB</span> </label>
                            <div class="col-md-9">
                                @if (isset($cellLine->mycoplasma_contamination))
                                    <div class="input-group">
                                        <input type="file" name="mycoplasma_contamination" id="mycoplasma_contamination" class="form-control">
                                        <div class="input-group-addon">
                                            <a target="_blank" href="{{ asset('uploads/mycoplasma-contaminations/'.$cellType->mycoplasma_contamination) }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <input type="file" name="mycoplasma_contamination" id="mycoplasma_contamination" class="form-control">
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Transcriptomics: </label>
                            <div class="col-md-9">
                                @if (isset($cellLine->transcriptomics))
                                    <div class="input-group">
                                        <input type="file" name="transcriptomics" id="transcriptomics" class="form-control">
                                        <div class="input-group-addon">
                                            <a target="_blank" href="{{ asset('uploads/transcriptomics/'.$cellLine->transcriptomics) }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <input type="file" name="transcriptomics" id="transcriptomics" class="form-control">
                                @endif
                            </div>
                        </div>

                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Note: </label>
                            <div class="col-md-9">
                                <select name="note" id="note" class="form-control">
                                    <option value="">---</option>
                                    @foreach ($note as $n)
                                        <option @if( isset($cellLine) && $cellLine->note_id == $n->id ) {{ 'selected' }} @endif value="{{ $n->id }}">{{ $n->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Aliquots: </label>
                            <div class="col-md-9">
                                <select name="aliquet" id="aliquet" class="form-control">
                                    <option value="">---</option>
                                    @foreach ($aliquet as $a)
                                        <option @if( isset($cellLine) && $cellLine->aliquate_id == $a->id ) {{ 'selected' }} @endif value="{{ $a->id }}">{{ $a->passenger_number }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row form-group">
                            <div class="col-md-12 text-right">
                                <button class="btn btn-primary btn-sm" type="submit">{{ isset($cellLine) ? "Update" : "Save" }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection