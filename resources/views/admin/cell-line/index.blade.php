@extends('layouts.admin-panel')

@section('title', 'Cell Lines')

@section('page_styles')
    <link rel="stylesheet" href="{{ asset('/css/plugins/sweetalert/sweetalert.css') }}" />
@endsection

@section('page_scripts')
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $(document).on('click', '.btn-delete-cell-line', function(e){
            e.preventDefault();
            var URL = $(this).attr('href');

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this again!",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No!",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function(){
                $.ajax({
                    type: "POST",
                    url: URL,
                    data: {
                        _token: "{{ csrf_token() }}",
                        _method: 'DELETE'
                    },
                    success: function(response){
                        console.log(response);
                        if (response.success){
                            swal("Deleted!", "Cell Line has been deleted.", "success");
                            tbl.fnDraw();
                        } else {
                            swal("Error...!", response.message, "error");
                        }
                    },
                });
            });

        });
        var tbl = $('#tblCellLine').dataTable({
            processing: true,
            serverSide: true,
            cache: false,
            ajax: {
                url: '{{ url('data/cell-line') }}',
                type: "POST",
                dataSrc: "data",
                data: {
                    _token: '{{ csrf_token() }}'
                }
            },
            columns: [
                { data: "id", searchable: false, sortable: false, visible: false },
                { data: "cell_line_label" },
                { data: "animal_class", name: 'ac.class_name' },
                { data: "subgroup", name: 'sg.name' },
                { data: "species", name: 'sp.name' },
                { data: "common_name", name: 'cn.name' },
                { data: "tissue_type", name: 'tt.type' },
                { data: "cell_type", name: 'ct.type' },
                { data: "note_title", name: 'n.title' },
                { data: "aliquate_passenger_number", name: 'a.passenger_number' },
                { data: 'action', searchable: false, sortable: false, className: "text-right" }
            ]
        });
    </script>
@endsection

@section('page_header')
    <h1>Cell Lines</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active"><strong>Cell Lines</strong></li>
    </ol>
@endsection

@section('page_content')
    @if (Session::has('cell_line_create'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable">
                    <strong>Success...</strong>
                    Cell Line inserted successfully...
                    <button data-dismiss="alert" class="close">&times;</button>
                </div>
            </div>
        </div>
        @php Session::forget('cell_line_create') @endphp
    @endif
    @if (Session::has('cell_line_update'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable">
                    <strong>Success...</strong>
                    Cell Line updated successfully...
                    <button data-dismiss="alert" class="close">&times;</button>
                </div>
            </div>
        </div>
        @php Session::forget('cell_line_update') @endphp
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Cell Lines</h5>
                    <div class="ibox-tools">
                        <a href="{{ route('cell-line.create') }}">
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered" id="tblCellLine">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Cell Lines</th>
                                        <th>Animal Class</th>
                                        <th>Subgroup</th>
                                        <th>Species Name</th>
                                        <th>Common Name</th>
                                        <th>Tissue Type</th>
                                        <th>Cell Type</th>
                                        <th>Note</th>
                                        <th>Aliquots</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection