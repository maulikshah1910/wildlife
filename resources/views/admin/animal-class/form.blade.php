@extends('layouts.admin-panel')

@section('title', 'Animal Classes')

@section('page_styles')
@endsection

@section('page_scripts')
    <script>
        $('#frm').validate({
            rules: {
                class_name: "required"
            }
        });
    </script>
@endsection

@section('page_header')
    <h1>Animal Classes</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('animal-class.index') }}">Animal Classes</a></li>
        <li class="breadcrumb-item active"><strong>{{ isset($animalClass) ? "Edit Animal Class" : "Create Animal Class" }}</strong></li>
    </ol>
@endsection

@section('page_content')
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>{{ isset($animalClass) ? "Edit Animal Class" : "Create Animal Class" }}</h5>
                    <div class="ibox-tools">
                        <a href="{{ route('animal-class.index') }}">
                            <i class="fa fa-long-arrow-left"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" id="frm" action="{{ isset($animalClass) ? route('animal-class.update', $animalClass->id) : route('animal-class.store') }}" method="post">
                        @csrf
                        @if (isset($animalClass))
                            {{ method_field('PUT') }}
                            <input type="hidden" name="id" value="{{ $animalClass->id }}" />
                        @endif
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Class Name: </label>
                            <div class="col-md-9">
                                <input type="text" name="class_name" id="class_name" class="form-control" value="{{ isset($animalClass) ? $animalClass->class_name : "" }}" />
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row form-group">
                            <div class="col-md-12 text-right">
                                <button class="btn btn-primary btn-sm" type="submit">{{ isset($animalClass) ? "Update" : "Save" }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection