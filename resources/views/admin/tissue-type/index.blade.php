@extends('layouts.admin-panel')

@section('title', 'Tissue Types')

@section('page_styles')
@endsection

@section('page_scripts')
    <script>
        $('#tblTissueType').dataTable({
            processing: true,
            serverSide: true,
            cache: false,
            ajax: {
                url: '{{ url('data/tissue-type') }}',
                type: "POST",
                dataSrc: "data",
                data: {
                    _token: '{{ csrf_token() }}'
                }
            },
            columns: [
                { data: "id", searchable: false, sortable: false, visible: false },
                { data: "type" },
                { data: "cellLineCount", className: "text-right" },
                { data: 'action', searchable: false, sortable: false, className: "text-right" }
            ]
        });
    </script>
@endsection

@section('page_header')
    <h1>Tissue Type</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active"><strong>Tissue Types</strong></li>
    </ol>
@endsection

@section('page_content')
    @if (Session::has('tissuetype_create'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable">
                    <strong>Success...</strong>
                    Tissue Type inserted successfully...
                    <button data-dismiss="alert" class="close">&times;</button>
                </div>
            </div>
        </div>
        @php Session::forget('tissuetype_create') @endphp
    @endif
    @if (Session::has('tissuetype_update'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable">
                    <strong>Success...</strong>
                    Tissue Type updated successfully...
                    <button data-dismiss="alert" class="close">&times;</button>
                </div>
            </div>
        </div>
        @php Session::forget('tissuetype_update') @endphp
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Tissue Types</h5>
                    <div class="ibox-tools">
                        <a href="{{ route('tissue-type.create') }}">
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered" id="tblTissueType">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Type</th>
                                        <th>Cell Lines</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection