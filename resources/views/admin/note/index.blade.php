@extends('layouts.admin-panel')

@section('title', 'Notes')

@section('page_styles')
@endsection

@section('page_scripts')
    <script>
        $('#tblNotes').dataTable({
            processing: true,
            serverSide: true,
            cache: false,
            ajax: {
                url: '{{ url('data/note') }}',
                type: "POST",
                dataSrc: "data",
                data: {
                    _token: '{{ csrf_token() }}'
                }
            },
            columns: [
                { data: "id", searchable: false, sortable: false, visible: false },
                { data: "title" },
                { data: "link" },
                { data: "cellLineCount", className: "text-right" },
                { data: 'action', searchable: false, sortable: false, className: "text-right" }
            ]
        });
    </script>
@endsection

@section('page_header')
    <h1>Notes</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active"><strong>Notes</strong></li>
    </ol>
@endsection

@section('page_content')
    @if (Session::has('note_create'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable">
                    <strong>Success...</strong>
                    Note inserted successfully...
                    <button data-dismiss="alert" class="close">&times;</button>
                </div>
            </div>
        </div>
        @php Session::forget('note_create') @endphp
    @endif
    @if (Session::has('note_update'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable">
                    <strong>Success...</strong>
                    Note updated successfully...
                    <button data-dismiss="alert" class="close">&times;</button>
                </div>
            </div>
        </div>
        @php Session::forget('note_update') @endphp
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Notes</h5>
                    <div class="ibox-tools">
                        <a href="{{ route('notes.create') }}">
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered" id="tblNotes">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Link</th>
                                        <th>Cell Lines</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection