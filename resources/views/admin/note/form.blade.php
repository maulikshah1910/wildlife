@extends('layouts.admin-panel')

@section('title', 'Notes')

@section('page_styles')
@endsection

@section('page_scripts')
    <script>
        $('#frm').validate({
            rules: {
                title: "required",
                link: {
                    "required": true,
                }
            }
        });
    </script>
@endsection

@section('page_header')
    <h1>Notes</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('notes.index') }}">Notes</a></li>
        <li class="breadcrumb-item active"><strong>{{ isset($note) ? "Edit Note" : "Create Note" }}</strong></li>
    </ol>
@endsection

@section('page_content')
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>{{ isset($note) ? "Edit Note" : "Create Note" }}</h5>
                    <div class="ibox-tools">
                        <a href="{{ route('notes.index') }}">
                            <i class="fa fa-long-arrow-left"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" id="frm" action="{{ isset($note) ? route('notes.update', $note->id) : route('notes.store') }}" method="post">
                        @csrf
                        @if (isset($note))
                            {{ method_field('PUT') }}
                            <input type="hidden" name="id" value="{{ $note->id }}" />
                        @endif
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Title: </label>
                            <div class="col-md-9">
                                <input type="text" name="title" id="title" class="form-control" value="{{ isset($note) ? $note->title : "" }}" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Link: </label>
                            <div class="col-md-9">
                                <input type="text" name="link" id="link" class="form-control" value="{{ isset($note) ? $note->link : "" }}" />
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row form-group">
                            <div class="col-md-12 text-right">
                                <button class="btn btn-primary btn-sm" type="submit">{{ isset($note) ? "Update" : "Save" }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection