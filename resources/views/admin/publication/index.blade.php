@extends('layouts.admin-panel')

@section('title', 'Publications')

@section('page_styles')
@endsection

@section('page_scripts')
    <script>
        $('#tblPublication').dataTable({
            processing: true,
            serverSide: true,
            cache: false,
            ajax: {
                url: '{{ url('data/publication') }}',
                type: "POST",
                dataSrc: "data",
                data: {
                    _token: '{{ csrf_token() }}'
                }
            },
            columns: [
                { data: "id", searchable: false, sortable: false, visible: false },
                { data: "title" },
                { data: "link" },
                { data: "cell_line_label" },
                { data: 'action', searchable: false, sortable: false, className: 'text-right' }
            ]
        });
    </script>
@endsection

@section('page_header')
    <h1>Publications</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active"><strong>Publications</strong></li>
    </ol>
@endsection

@section('page_content')
    @if (Session::has('publication_create'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable">
                    <strong>Success...</strong>
                    Publication inserted successfully...
                    <button data-dismiss="alert" class="close">&times;</button>
                </div>
            </div>
        </div>
        @php Session::forget('publication_create') @endphp
    @endif
    @if (Session::has('publication_update'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable">
                    <strong>Success...</strong>
                    Publication updated successfully...
                    <button data-dismiss="alert" class="close">&times;</button>
                </div>
            </div>
        </div>
        @php Session::forget('publication_update') @endphp
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Publications</h5>
                    <div class="ibox-tools">
                        <a href="{{ route('publication.create') }}">
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered" id="tblPublication">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Link</th>
                                    <th>Cell Line</th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection