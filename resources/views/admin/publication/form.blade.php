@extends('layouts.admin-panel')

@section('title', 'Publications')

@section('page_styles')
    <link rel="stylesheet" href="{{ asset('css/plugins/select2/select2.min.css') }}" />

    <style>
        #frm label.error {
            position: absolute;
            bottom: 0;
            top: auto;
            right: 2.5%;
            z-index: 999;
            font-weight: 600;
        }
    </style>
@endsection

@section('page_scripts')
    <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
    <script>
        $('#frm').find('select').select2();
        $('#frm').validate({
            rules: {
                title:{
                    required: true,
                },
                link: {
                    required: true,
                    url: true
                },
                cell_line: {
                    required: true
                },
            }
        });
    </script>
@endsection

@section('page_header')
    <h1>Publications</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('publication.index') }}">Publications</a></li>
        <li class="breadcrumb-item active"><strong>{{ isset($publication) ? "Edit Publication" : "Create Publication" }}</strong></li>
    </ol>
@endsection

@section('page_content')
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>{{ isset($publication) ? "Edit Publication" : "Create Publication" }}</h5>
                    <div class="ibox-tools">
                        <a href="{{ route('publication.index') }}">
                            <i class="fa fa-long-arrow-left"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" id="frm" action="{{ isset($publication) ? route('publication.update', $publication->id) : route('publication.store') }}" method="post">
                        @csrf
                        @if (isset($publication))
                            {{ method_field('PUT') }}
                            <input type="hidden" name="id" value="{{ $publication->id }}" />
                        @endif
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Title: </label>
                            <div class="col-md-9">
                                <input type="text" name="title" id="title" class="form-control" value="{{ isset($publication) ? $publication->title : "" }}" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Link: </label>
                            <div class="col-md-9">
                                <input type="text" name="link" id="link" class="form-control" value="{{ isset($publication) ? $publication->link : "" }}" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Cell Line: </label>
                            <div class="col-md-9">
                                <select name="cell_line" id="cell_line" class="form-control">
                                    <option value="">---</option>
                                    @foreach ($cellLines as $c)
                                        <option @if( isset($publication) && $publication->cell_line_id == $c->id ) {{ 'selected' }} @endif value="{{ $c->id }}">{{ $c->cell_line_label }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row form-group">
                            <div class="col-md-12 text-right">
                                <button class="btn btn-primary btn-sm" type="submit">{{ isset($publication) ? "Update" : "Save" }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection