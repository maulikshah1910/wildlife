@extends('layouts.admin-panel')

@section('title', 'Common Names')

@section('page_styles')
@endsection

@section('page_scripts')
    <script>
        $('#frm').validate({
            rules: {
                name: "required"
            }
        });
    </script>
@endsection

@section('page_header')
    <h1>Animal Classes</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('common-name.index') }}">Common Names</a></li>
        <li class="breadcrumb-item active"><strong>{{ isset($commonName) ? "Edit Common Name" : "Create Common Name" }}</strong></li>
    </ol>
@endsection

@section('page_content')
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>{{ isset($commonName) ? "Edit Common Name" : "Create Common Name" }}</h5>
                    <div class="ibox-tools">
                        <a href="{{ route('common-name.index') }}">
                            <i class="fa fa-long-arrow-left"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" id="frm" action="{{ isset($commonName) ? route('common-name.update', $commonName->id) : route('common-name.store') }}" method="post">
                        @csrf
                        @if (isset($commonName))
                            {{ method_field('PUT') }}
                            <input type="hidden" name="id" value="{{ $commonName->id }}" />
                        @endif
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Name: </label>
                            <div class="col-md-9">
                                <input type="text" name="name" id="name" class="form-control" value="{{ isset($commonName) ? $commonName->name : "" }}" />
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row form-group">
                            <div class="col-md-12 text-right">
                                <button class="btn btn-primary btn-sm" type="submit">{{ isset($commonName) ? "Update" : "Save" }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection