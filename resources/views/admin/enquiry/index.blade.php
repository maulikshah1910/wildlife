@extends('layouts.admin-panel')

@section('title', 'Contact Enquiries')

@section('page_styles')
@endsection

@section('page_scripts')
    <script>
        $('#tblEnquiry').dataTable({
            processing: true,
            serverSide: true,
            cache: false,
            order: [
                [ 1 , "DESC" ]
            ],
            ajax: {
                url: '{{ url('data/enquiry') }}',
                type: "POST",
                dataSrc: "data",
                data: {
                    _token: '{{ csrf_token() }}'
                }
            },
            columns: [
                { data: "id", searchable: false, sortable: false, visible: false },
                { data: "created_at" },
                { data: "name" },
                { data: "email" },
                { data: "message", sortable: false },
            ]
        });
    </script>
@endsection

@section('page_header')
    <h1>Contact Enquiries</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active"><strong>Contact Enquiries</strong></li>
    </ol>
@endsection

@section('page_content')
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Contact Enquiries</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered" id="tblEnquiry">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Enquiry On</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Message</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection