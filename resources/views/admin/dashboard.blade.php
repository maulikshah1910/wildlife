@extends('layouts.admin-panel')

@section('title', 'Dashboard')

@section('page_styles')
@endsection

@section('page_scripts')
@endsection

@section('page_header')
    <h1>Dashboard</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-dashboard"></i> Home</li>
        <li class="breadcrumb-item active"><strong>Dashboard</strong></li>
    </ol>

@endsection

@section('page_content')
    <div class="row">
        <div class="col-md-3">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Animal Classes</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-9">
                            <h1 class="no-margins">{{ isset($animalClassCount) ? $animalClassCount : "0" }}</h1>
                        </div>
                        <div class="col-md-3 text-right">
                            <a href="{{ route('animal-class.index') }}" class="btn btn-primary">View <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>SubGroups</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-9">
                            <h1 class="no-margins">{{ isset($subGroupCount) ? $subGroupCount : "0" }}</h1>
                        </div>
                        <div class="col-md-3 text-right">
                            <a href="{{ route('subgroup.index') }}" class="btn btn-primary">View <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Species</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-9">
                            <h1 class="no-margins">{{ isset($speciesCount) ? $speciesCount : "0" }}</h1>
                        </div>
                        <div class="col-md-3 text-right">
                            <a href="{{ route('species.index') }}" class="btn btn-primary">View <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Common Names</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-9">
                            <h1 class="no-margins">{{ isset($commonNameCount) ? $commonNameCount : "0" }}</h1>
                        </div>
                        <div class="col-md-3 text-right">
                            <a href="{{ route('common-name.index') }}" class="btn btn-primary">View <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Tissue Types</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-9">
                            <h1 class="no-margins">{{ isset($tissueTypeCount) ? $tissueTypeCount : "0" }}</h1>
                        </div>
                        <div class="col-md-3 text-right">
                            <a href="{{ route('tissue-type.index') }}" class="btn btn-primary">View <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Cell Types</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-9">
                            <h1 class="no-margins">{{ isset($cellTypeCount) ? $cellTypeCount : "0" }}</h1>
                        </div>
                        <div class="col-md-3 text-right">
                            <a href="{{ route('cell-type.index') }}" class="btn btn-primary">View <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Notes</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-9">
                            <h1 class="no-margins">{{ isset($notesCount) ? $notesCount : "0" }}</h1>
                        </div>
                        <div class="col-md-3 text-right">
                            <a href="{{ route('notes.index') }}" class="btn btn-primary">View <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Aliquets</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-9">
                            <h1 class="no-margins">{{ isset($aliquetCount) ? $aliquetCount : "0" }}</h1>
                        </div>
                        <div class="col-md-3 text-right">
                            <a href="{{ route('aliquet.index') }}" class="btn btn-primary">View <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Cell Lines</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-9">
                            <h1 class="no-margins">{{ isset($cellLineCount) ? $cellLineCount : "0" }}</h1>
                        </div>
                        <div class="col-md-3 text-right">
                            <a href="{{ route('cell-line.index') }}" class="btn btn-primary">View <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection