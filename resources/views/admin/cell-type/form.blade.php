@extends('layouts.admin-panel')

@section('title', 'Cell Types')

@section('page_styles')
@endsection

@section('page_scripts')
    <script>
        $('#frm').validate({
            rules: {
                type: "required"
            }
        });
    </script>
@endsection

@section('page_header')
    <h1>Cell Types</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('cell-type.index') }}">Cell Types</a></li>
        <li class="breadcrumb-item active"><strong>{{ isset($cellType) ? "Edit Cell Type" : "Create Cell Type" }}</strong></li>
    </ol>
@endsection

@section('page_content')
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>{{ isset($cellType) ? "Edit Cell Type" : "Create Cell Type" }}</h5>
                    <div class="ibox-tools">
                        <a href="{{ route('cell-type.index') }}">
                            <i class="fa fa-long-arrow-left"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" id="frm" action="{{ isset($cellType) ? route('cell-type.update', $cellType->id) : route('cell-type.store') }}" method="post">
                        @csrf
                        @if (isset($cellType))
                            {{ method_field('PUT') }}
                            <input type="hidden" name="id" value="{{ $cellType->id }}" />
                        @endif
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Type: </label>
                            <div class="col-md-9">
                                <input type="text" name="type" id="type" class="form-control" value="{{ isset($cellType) ? $cellType->type : "" }}" />
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row form-group">
                            <div class="col-md-12 text-right">
                                <button class="btn btn-primary btn-sm" type="submit">{{ isset($cellType) ? "Update" : "Save" }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection