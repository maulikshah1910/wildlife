@extends('layouts.admin-panel')

@section('title', 'My Profile')


@section('page_header')
    <h1>My Profile</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active"><strong>My Profile</strong></li>
    </ol>
@endsection

@section('page_scripts')
    <script>
        $('#frmChangePass').validate({
            rules: {
                oldpass: {
                    required: true,
                },
                newpass: {
                    required: true,
                },
                confpass:{
                    required: true,
                    equalTo: '#newpass'
                }
            },
            messages: {
                oldpass: {
                    required: 'Please enter Old Password'
                },
                newpass: {
                    required: 'Please enter New Password'
                },
                confpass: {
                    required: 'Please enter Confirm Password',
                    equalTo: 'New and Confirm Passworm must match'
                }
            }
        });
    </script>
@endsection

@section('page_content')
    @if (Session::has('password_update'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable">
                    <strong>Success...</strong><br />
                    Your password is updated successfully...
                    <button data-dismiss="alert" class="close">&times;</button>
                </div>
            </div>
        </div>
        @php Session::forget('password_update') @endphp
    @endif
    @if (Session::has('password_mismatch'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissable">
                    <strong>Error...</strong><br />
                    Your New and Confirm Password did not match... Please try again...
                    <button data-dismiss="alert" class="close">&times;</button>
                </div>
            </div>
        </div>
        @php Session::forget('password_mismatch') @endphp
    @endif
    @if (Session::has('invalid_oldpass'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissable">
                    <strong>Error...</strong><br />
                    You have given wrong password... Please enter valid password.
                    <button data-dismiss="alert" class="close">&times;</button>
                </div>
            </div>
        </div>
        @php Session::forget('invalid_oldpass') @endphp
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Manage Profile</h5>
                </div>
                <div class="ibox-content">
                    <form action="{{ route('change-password') }}" class="form-horizontal" id="frmChangePass" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="email" class="col-md-3 col-form-label">Email: </label>
                            <div class="col-md-9">
                                <input type="text" name="email" disabled readonly id="email" class="form-control" value="{{ Auth::user()->email }}" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="oldpass" class="col-md-3 col-form-label">Old Password: </label>
                            <div class="col-md-9">
                                <input type="password" name="oldpass" id="oldpass" class="form-control" value="" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="newpass" class="col-md-3 col-form-label">New Password: </label>
                            <div class="col-md-9">
                                <input type="password" name="newpass" id="newpass" class="form-control" value="" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="confpass" class="col-md-3 col-form-label">Confirm Password: </label>
                            <div class="col-md-9">
                                <input type="password" name="confpass" id="confpass" class="form-control" value="" />
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row form-group">
                            <div class="col-md-12 text-right">
                                <button class="btn btn-primary btn-sm" type="submit">Change Password</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection