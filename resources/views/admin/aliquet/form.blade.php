@extends('layouts.admin-panel')

@section('title', 'Aliquots')

@section('page_styles')
    <link rel="stylesheet" href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" />
@endsection

@section('page_scripts')
    <script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
    <script>

        $('.date-control').datepicker({
            todayBtn: false,
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });
        $('#frm').validate({
            rules: {
                passenger_number: {
                    required: true,
                    number: true
                },
                cryopreservation_date: "required"
            }
        });
    </script>
@endsection

@section('page_header')
    <h1>Aliquots</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('aliquot.index') }}">Aliquots</a></li>
        <li class="breadcrumb-item active"><strong>{{ isset($aliquet) ? "Edit Aliquots" : "Create Aliquots" }}</strong></li>
    </ol>
@endsection

@section('page_content')
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>{{ isset($aliquet) ? "Edit Aliquots" : "Create Aliquots" }}</h5>
                    <div class="ibox-tools">
                        <a href="{{ route('aliquot.index') }}">
                            <i class="fa fa-long-arrow-left"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" id="frm" action="{{ isset($aliquet) ? route('aliquot.update', $aliquet->id) : route('aliquot.store') }}" method="post">
                        @csrf
                        @if (isset($aliquet))
                            {{ method_field('PUT') }}
                            <input type="hidden" name="id" value="{{ $aliquet->id }}" />
                        @endif
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Passenger Number: </label>
                            <div class="col-md-9">
                                <input type="text" name="passenger_number" id="passenger_number" class="form-control" value="{{ isset($aliquet) ? $aliquet->passenger_number : "" }}" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Cryopreservation Date: </label>
                            <div class="col-md-9">
                                <input type="text" name="cryopreservation_date" id="cryopreservation_date" class="form-control date-control" readonly value="{{ isset($aliquet) ? substr($aliquet->cryopreservation_date, 0, 10): "" }}" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="" class="col-md-3 col-form-label">Status: </label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="radio radio-primary">
                                            <input type="radio" name="status" id="status_1" value="1" {{ ( isset($aliquet) && $aliquet->status == 1 ) ? 'checked' : ( (!isset($aliquet)) ? 'checked' : '' ) }} />
                                            <label for="status_1">In Stock</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 radio radio-danger">
                                        <div class="radio radio-danger">
                                            <input type="radio" name="status" id="status_2" value="2" {{ ( isset($aliquet) && $aliquet->status == 2 ) ? 'checked' : ''  }} />
                                            <label for="status_2">Out of Stock</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row form-group">
                            <div class="col-md-12 text-right">
                                <button class="btn btn-primary btn-sm" type="submit">{{ isset($aliquet) ? "Update" : "Save" }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection