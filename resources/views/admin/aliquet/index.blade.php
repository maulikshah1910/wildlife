@extends('layouts.admin-panel')

@section('title', 'Aliquots')

@section('page_styles')
@endsection

@section('page_scripts')
    <script>
        $('#tblAliquet').dataTable({
            processing: true,
            serverSide: true,
            cache: false,
            ajax: {
                url: '{{ url('data/aliquet') }}',
                type: "POST",
                dataSrc: "data",
                data: {
                    _token: '{{ csrf_token() }}'
                }
            },
            columns: [
                { data: "id", searchable: false, sortable: false, visible: false },
                { data: "passenger_number" },
                { data: "cryopreservation_date" },
                { data: "status" },
                { data: "cellLineCount", className: "text-right" },
                { data: 'action', searchable: false, sortable: false, className: 'text-right' }
            ]
        });
    </script>
@endsection

@section('page_header')
    <h1>Aliquots</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active"><strong>Aliquots</strong></li>
    </ol>
@endsection

@section('page_content')
    @if (Session::has('aliquet_create'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable">
                    <strong>Success...</strong>
                    Aliquots inserted successfully...
                    <button data-dismiss="alert" class="close">&times;</button>
                </div>
            </div>
        </div>
        @php Session::forget('aliquet_create') @endphp
    @endif
    @if (Session::has('aliquet_update'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable">
                    <strong>Success...</strong>
                    Aliquots updated successfully...
                    <button data-dismiss="alert" class="close">&times;</button>
                </div>
            </div>
        </div>
        @php Session::forget('aliquet_update') @endphp
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Aliquets</h5>
                    <div class="ibox-tools">
                        <a href="{{ route('aliquot.create') }}">
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered" id="tblAliquet">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Passenger Numbers</th>
                                        <th>Cryopreservation Date</th>
                                        <th>Status</th>
                                        <th>Cell Lines</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection