<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

        <title>@yield('page-title') | WildLife</title>

        <link rel="shortcut icon" href="{{ asset('front/img/favicon.png') }}" type="image/x-icon">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}" />
        <!-- animate CSS -->
        <link rel="stylesheet" href="{{ asset('front/css/animate.css') }}" />
        <!-- owl carousel CSS -->
        <link rel="stylesheet" href="{{ asset('front/css/owl.carousel.min.css') }}" />
        <!-- themify CSS -->
        <link rel="stylesheet" href="{{ asset('front/css/themify-icons.css') }}" />
        <!-- flaticon CSS -->
        <link rel="stylesheet" href="{{ asset('front/css/flaticon.css') }}" />
        <!-- font awesome CSS -->
        <link rel="stylesheet" href="{{ asset('front/css/magnific-popup.css') }}" />
        <!-- swiper CSS -->
        <link rel="stylesheet" href="{{ asset('front/css/slick.css') }}" />
        <!-- style CSS -->
        <link rel="stylesheet" href="{{ asset('front/css/style.css') }}" />

        <link href="{{ asset('front/css/font-awesome.min.css') }}" rel="stylesheet">

        <style>
            .navbar-brand img { max-width: 125px; }
            .breadcrumb_bg { background-image: url('{{ asset('images/banner-bg.webp') }}'); }
        </style>
        @yield ('page-styles')
    </head>
    <body>
        <header class="main_menu home_menu">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <nav class="navbar navbar-expand-md navbar-light">
                            <a class="navbar-brand" href="{{ route('front_page') }}">
                                {{--<img src="{{ asset('front/img/logo.png') }}" alt="logo">--}}
                                <img src="{{ asset('images/logo.png') }}" alt="logo">
                            </a>
                            <button
                                    class="navbar-toggler"
                                    type="button"
                                    data-toggle="collapse"
                                    data-target="#navbarSupportedContent"
                                    aria-controls="navbarSupportedContent"
                                    aria-expanded="false"
                                    aria-label="Toggle navigation">
                                <span class="ti-menu"></span>
                            </button>
                            <div class="collapse navbar-collapse main-menu-item justify-content-center" id="navbarSupportedContent">
                                <ul class="navbar-nav align-items-center">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('front_page') }}">Home</a>
                                    </li>
                                    {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link" href="{{ route('about') }}">About</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link" href="{{ route('contact') }}">Contact</a>--}}
                                    {{--</li>--}}
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('search') }}">Search</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="social_icon d-none d-lg-block">
                                <a href="{{ url('login') }}"><i class="ti-user"></i> Admin Login</a>
                                {{--<a href="#"><i class="ti-facebook"></i></a>--}}
                                {{--<a href="#"><i class="ti-twitter-alt"></i></a>--}}
                                {{--<a href="#"><i class="ti-dribbble"></i></a>--}}
                                {{--<a href="#"><i class="ti-instagram"></i></a>--}}
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        @if (Route::is('front_page'))
        <section class="banner_part">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-7 offset-md-1">
                        <div class="banner_text">
                            <div class="banner_text_iner">
                                <h1>ARI-TOX Marine<br />Wildlife Cell Bank</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @else
        <section class="breadcrumb breadcrumb_bg align-items-center">
            <div class="container">
                <div class="row align-items-center justify-content-between">
                    <div class="col-sm-6">
                        <div class="breadcrumb_tittle">
                            @yield('breadcrumbs')
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @endif

        @yield('page-content')

        <footer class="footer-area">
            <div class="container">
                <div class="row justify-content-between">

                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright_part_text text-center">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="footer-text m-0">
                                        Copyrights &copy; {{ \Carbon\Carbon::now()->format('Y') }} AriTOX
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    <!-- jquery plugins here-->
    <script src="{{ asset('front/js/jquery-1.12.1.min.js') }}"></script>
    <!-- popper js -->
    <script src="{{ asset('front/js/popper.min.js') }}"></script>
    <!-- bootstrap js -->
    <script src="{{ asset('front/js/bootstrap.min.js') }}"></script>
    <!-- easing js -->
    <script src="{{ asset('front/js/jquery.magnific-popup.js') }}"></script>
    <!-- isotope js -->
    <script src="{{ asset('front/js/isotope.pkgd.min.js') }}"></script>
    <!-- particles js -->
    <script src="{{ asset('front/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('front/js/jquery.nice-select.min.js') }}"></script>
    <!-- custom js -->
    <script src="{{ asset('front/js/custom.js') }}"></script>

    @yield('page-scripts')
    </body>
</html>