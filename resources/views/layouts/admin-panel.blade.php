<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title') | {{ config('app.name', 'WildLife') }}</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('css/plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

    @yield('page_styles')
</head>
<body>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="block m-t-xs font-bold">{{ Auth::user()->name }} <b class="caret"></b></span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a class="dropdown-item" href="{{ url('profile') }}">Profile</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="{{ url('logout') }}">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element"> ST </div>
                </li>
                <li class="{{ Request::is('dashboard') ? 'active' : '' }}">
                    <a href="{{ url('dashboard') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span> </a>
                </li>
                <li class="{{ Request::is('animal-class') || Request::is('animal-class/*') ? 'active' : '' }}">
                    <a href="{{ url('animal-class') }}"><i class="fa fa-cog fa-spin"></i> <span class="nav-label">Animal Classes</span></a>
                </li>
                <li class="{{ Request::is('subgroup') || Request::is('subgroup/*') ? 'active' : '' }}">
                    <a href="{{ url('subgroup') }}"><i class="fa fa-flash "></i> <span class="nav-label">SubGroups</span></a>
                </li>
                <li class="{{ Request::is('species') || Request::is('species/*') ? 'active' : '' }}">
                    <a href="{{ url('species') }}"><i class="fa fa-taxi "></i> <span class="nav-label">Species</span></a>
                </li>
                <li class="{{ Request::is('common-name') || Request::is('common-name/*') ? 'active' : '' }}">
                    <a href="{{ url('common-name') }}"><i class="fa fa-credit-card"></i> <span class="nav-label">Common Names</span></a>
                </li>
                <li class="{{ Request::is('tissue-type') || Request::is('tissue-type/*') ? 'active' : '' }}">
                    <a href="{{ url('tissue-type') }}"><i class="fa fa-address-book"></i> <span class="nav-label">Tissue Types</span></a>
                </li>
                <li class="{{ Request::is('cell-type') || Request::is('cell-type/*') ? 'active' : '' }}">
                    <a href="{{ url('cell-type') }}"><i class="fa fa-address-book"></i> <span class="nav-label">Cell Types</span></a>
                </li>
                <li class="{{ Request::is('notes') || Request::is('notes/*') ? 'active' : '' }}">
                    <a href="{{ url('notes') }}"><i class="fa fa-address-book"></i> <span class="nav-label">Notes</span></a>
                </li>
                <li class="{{ Request::is('aliquot') || Request::is('aliquot/*') ? 'active' : '' }}">
                    <a href="{{ url('aliquot') }}"><i class="fa fa-address-book"></i> <span class="nav-label">Aliquots</span></a>
                </li>
                <li class="{{ Request::is('cell-line') || Request::is('cell-line/*') ? 'active' : '' }}">
                    <a href="{{ url('cell-line') }}"><i class="fa fa-address-book"></i> <span class="nav-label">Cell Lines</span></a>
                </li>
                <li class="{{ Request::is('publication') || Request::is('publication/*') ? 'active' : '' }}">
                    <a href="{{ url('publication') }}"><i class="fa fa-files-o"></i> <span class="nav-label">Publication</span></a>
                </li>
                <li class="{{ Request::is('enquiry') || Request::is('enquiry/*') ? 'active' : '' }}">
                    <a href="{{ url('enquiry') }}"><i class="fa fa-address-book"></i> <span class="nav-label">Contact Enquiries</span></a>
                </li>
                <li class="{{ Request::is('profile') || Request::is('profile/*') ? 'active' : '' }}">
                    <a href="{{ route('profile') }}"><i class="fa fa-user-circle"></i> <span class="nav-label">My Profile</span></a>
                </li>
                <li>
                    <a href="{{ url('logout') }}"><i class="fa fa-sign-out"></i> <span class="nav-label">Log Out</span></a>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top " role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <span class="m-r-sm text-muted welcome-message">Welcome to WildLife Admin Panel</span>
                    </li>
                    <li>
                        <span class="m-r-sm text-muted welcome-message">{{ Auth::user()->name }}</span>
                    </li>
                    <li>
                        <a href="{{url('logout')}}"> <i class="fa fa-sign-out"></i> Log out</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-md-12">
                @yield('page_header')
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            @yield('page_content')
        </div>

        <div class="footer">
            <div>
                <strong>Copyright</strong> {{env('APP_NAME', 'AriTOX')}} &copy; {{now()->year}}
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.js') }}"></script>
<script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

<script src="{{ asset('js/inspinia.js') }}"></script>
<script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>
<script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
<script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>

<script src="{{ asset('js/plugins/validate/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/plugins/validate/additional-methods.js') }}"></script>

@yield('page_scripts')
<script>
    $('[data-toggle="tooltip"]').tooltip();
    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' }
    });
    jQuery.validator.setDefaults({
        debug: true,
        success: "valid"
    });
</script>
</body>
</html>