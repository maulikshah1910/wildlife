$(document).on("click", ".btn-delete", function (e) {
    e.preventDefault();
    var url = $(this).attr("href");
    var id = $(this).data("id");
    swal({
        title: "Are you sure want to delete?",
        text: "You will may lose some data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true
    }, function () {
        alert('Now it can be deleted...');
        // $.ajax({
        //     url: url,
        //     type: 'POST',
        //     dataType: 'json',
        //     data: {id: id, _token: csrfToken},
        //     success: function (response) {
        //         if(response.success){
        //             afterDeleteSuccess(response);
        //         } else if(response.warning) {
        //             afterDeleteWarning(response);
        //         }
        //     },
        //     error: function () {
        //         afterDeleteError();
        //     }
        // });

    });
});